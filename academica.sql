-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-05-2020 a las 23:08:22
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `academica`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carreras`
--

CREATE TABLE `carreras` (
  `id_ca` varchar(50) DEFAULT NULL,
  `nombre_ca` varchar(50) DEFAULT NULL,
  `materias_ca` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `carreras`
--

INSERT INTO `carreras` (`id_ca`, `nombre_ca`, `materias_ca`) VALUES
(NULL, NULL, NULL),
(NULL, NULL, NULL),
(NULL, NULL, NULL),
(NULL, NULL, NULL),
(NULL, NULL, NULL),
(NULL, NULL, NULL),
(NULL, NULL, NULL),
(NULL, NULL, NULL),
(NULL, NULL, NULL),
(NULL, NULL, NULL),
(NULL, NULL, NULL),
(NULL, NULL, NULL),
(NULL, NULL, NULL),
(NULL, NULL, NULL),
(NULL, NULL, NULL),
(NULL, NULL, NULL),
(NULL, NULL, NULL),
(NULL, NULL, NULL),
(NULL, NULL, NULL),
(NULL, NULL, NULL),
(NULL, 'Enrique', NULL),
(NULL, 'Enrique', NULL),
(NULL, 'Enrique', NULL),
(NULL, 'Enrique', NULL),
(NULL, 'Enrique', NULL),
(NULL, 'Enrique', NULL),
(NULL, 'Enrique', NULL),
(NULL, 'Enrique', NULL),
(NULL, 'Enrique', NULL),
(NULL, 'Enrique', NULL),
(NULL, 'Enrique', NULL),
(NULL, 'Enrique', NULL),
(NULL, 'Enrique', NULL),
(NULL, 'Enrique', NULL),
(NULL, 'Enrique', NULL),
(NULL, 'Enrique', NULL),
(NULL, 'Enrique', NULL),
(NULL, 'Enrique', NULL),
(NULL, '2', NULL),
(NULL, '2', NULL),
(NULL, '2', NULL),
(NULL, '2', NULL),
('smis1010', 'Enrique Caballero', 'programacion lv ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docentes`
--

CREATE TABLE `docentes` (
  `id_doc` varchar(50) DEFAULT NULL,
  `nombre_doc` varchar(50) DEFAULT NULL,
  `apellido_doc` varchar(50) DEFAULT NULL,
  `titulo_doc` varchar(50) DEFAULT NULL,
  `telefono_doc` varchar(50) DEFAULT NULL,
  `direccion_doc` varchar(50) DEFAULT NULL,
  `ciudad_doc` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `docentes`
--

INSERT INTO `docentes` (`id_doc`, `nombre_doc`, `apellido_doc`, `titulo_doc`, `telefono_doc`, `direccion_doc`, `ciudad_doc`) VALUES
('smis573818', 'Jose Enrique', 'Herrera Caballero', 'bachillerato', '123456', 'san miguel', 'san miguel'),
('sm111', 'Omar', 'Herrera', 'Licenciado', '1234', 'san miguel', 'san miguel');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `codigo` varchar(10) NOT NULL,
  `nombres` varchar(50) NOT NULL,
  `lugar_nacimiento` varchar(30) NOT NULL,
  `fecha_nacimiento` varchar(30) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `puesto` varchar(20) NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`codigo`, `nombres`, `lugar_nacimiento`, `fecha_nacimiento`, `direccion`, `telefono`, `puesto`, `estado`) VALUES
('sm3011', 'Omar Herrera', 'Jucuapa,Usulutan', '10-10-1976', 'jucuapa', '123456', 'Supervisor', 1),
('smis573818', 'Emperatriz ', 'Moncagua', '26-11-2001', 'San Miguel', '00000', 'Gerente', 2),
('0001', 'Jose Enrique Herrera Cabellero', 'Jucuapa,Usulutan', '21-05-2000', 'San Miguel', '123', 'Estudiante', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudiantes`
--

CREATE TABLE `estudiantes` (
  `id` varchar(20) DEFAULT NULL,
  `nombres` varchar(50) DEFAULT NULL,
  `apellidos` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estudiantes`
--

INSERT INTO `estudiantes` (`id`, `nombres`, `apellidos`) VALUES
('smis573818', 'Jose', 'Herrera'),
('001', 'Jose Enrique', 'Herrera Caballero'),
('002', 'Enrique', 'Herrera'),
('0022', 'Omar Enrique', 'Herrera Caballero');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horarios`
--

CREATE TABLE `horarios` (
  `id_ho` varchar(50) DEFAULT NULL,
  `hora_ho` varchar(50) DEFAULT NULL,
  `aula_ho` varchar(50) DEFAULT NULL,
  `docente_ho` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `horarios`
--

INSERT INTO `horarios` (`id_ho`, `hora_ho`, `aula_ho`, `docente_ho`) VALUES
('sm301', '12:00 PM', 'ns31', 'Omar Herrera');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materias`
--

CREATE TABLE `materias` (
  `id_ma` varchar(50) DEFAULT NULL,
  `nombre_ma` varchar(50) DEFAULT NULL,
  `carrera_ma` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `materias`
--

INSERT INTO `materias` (`id_ma`, `nombre_ma`, `carrera_ma`) VALUES
('3', 'w', 'ee'),
('001', 'Omar', 'SOCIALES');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usuario` varchar(60) NOT NULL,
  `passw` varchar(60) NOT NULL,
  `nombre_usuario` varchar(100) NOT NULL,
  `nivel` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuario`, `passw`, `nombre_usuario`, `nivel`) VALUES
('admin', '827ccb0eea8a706c4c34a16891f84e7b', 'Emperatriz', 1),
('tecnico', '827ccb0eea8a706c4c34a16891f84e7b', 'Enrique Herrera', 5),
('terciario', '827ccb0eea8a706c4c34a16891f84e7b', 'omar', 4);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
